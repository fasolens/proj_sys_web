<?php
    session_start();

    if(isset($_POST['submit'])){

        $change = FALSE;

        if($_SESSION['first_name'] != $_POST['first_name_edit']){
            $change = TRUE;
            $first_name_edit = $_POST['first_name_edit'];
        } else
            $first_name_edit = $_SESSION['first_name'];

        if($_SESSION['last_name'] != $_POST['last_name_edit']){
            $change = TRUE;
            $last_name_edit = $_POST['last_name_edit'];
        } else
            $last_name_edit = $_SESSION['last_name'];

        if($_SESSION['email'] != $_POST['email_edit']){
            $change = TRUE;
            $email_edit = $_POST['email_edit'];
        } else
            $email_edit = $_SESSION['email'];

        $login = $_SESSION['login'];
        $pass = $_SESSION['pass'];
        
        if($change == TRUE){
            //CONNECT TO DB
            echo "Wystąpiły zmiany";

            require_once "connect.php";

            try {
                $connection = new mysqli($host, $db_user, $db_password, $db_name);
                if ($connection->connect_errno != 0) {
                    throw new Exception(mysqli_connect_errno());
                } else {
                        if ($connection->query("UPDATE users SET first_name = '$first_name_edit', last_name = '$last_name_edit', email = '$email_edit' WHERE login = '$login'")) {
                            $_SESSION['success_edit'] = true;
                            //echo "Zmieniono dane usera";
                            header('Location: login.php');
                            exit();
                        } else {
                            echo "Coś poszło nie tak. <a href='index1.php'> Spróbuj jeszcze raz. </a>";
                        }
                    }
                    $connection->close();
            } catch (Exception $e) {
                echo '<span style="color:red;">Błąd serwera!</span>';
                //echo 'Info' . $e;
            }
        } else
            echo "Brak zmian";
    }
?>
<!DOTYPE HTML>
<hmtl lang="pl">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <title>Strona główna</title>
        <style type="text/css">
            th{
                text-align: right;
            }
            h3 {
                text-align: center;
            }
        </style>
    </head>
    <body>
    <table cellpadding="5" cellspadding="10" align="center">
        <h3 align="center">Zmiana danych osobowych</h3>
        <form method="post" >
            <tr><th>Imie:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['first_name'])){
                        echo $_SESSION['first_name'];
                    }?>" name="first_name_edit"></td></tr>
            <tr><th>Nazwisko:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['last_name'])){
                        echo $_SESSION['last_name'];
                    }?>" name="last_name_edit"></td></tr>
            <tr><th>E-mail:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['email'])){
                        echo $_SESSION['email'];
                    }?>" name="email_edit"></td></tr>
            <tr><th></th><td>
                </td></tr></td></tr>
            <tr><td colspan="2" align="right"><input type="submit" value="Zapisz zmiany" name="submit"></td></tr>
        </form>
    </table>
    </body>
</hmtl>