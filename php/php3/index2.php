<?php
    session_start();

    if(isset($_POST['login'])) {

        $all_ok = true;

        $login = $_POST['login'];

        if ((strlen($login) < 5) || (strlen($login) > 30)) {
            $all_ok = false;
            $_SESSION['e_login'] = "Login musi posiadać od 5 do 30 znaków !";
        }

        if (ctype_alnum($login) == false) {
            $all_ok = false;
            $_SESSION['e_login'] = "Login może składać się tylko z liter i cyfr (bez polskich znaków) !";
        }

        $pass1 = $_POST['password1'];
        $pass2 = $_POST['password2'];

        if ((strlen($pass1) < 8) || (strlen($pass1) > 20)) {
            $all_ok = false;
            $_SESSION['e_pass'] = "Hasło musi posiadać od 8 do 20 znaków !";
        }

        if ($pass1 != $pass2) {
            $all_ok = false;
            $_SESSION['e_pass'] = "Podane hasło nie sa takie indentyczne !";
        }

        $first_name = $_POST['first_name'];

        if ((!empty($first_name)) && (ctype_upper($first_name[0]) == false)) {
            $all_ok = false;
            $_SESSION['e_first_name'] = "Imię musi rozpoczynać się wielką literą !";
        }

        $last_name = $_POST['last_name'];

        if ((!empty($last_name)) && (ctype_upper($last_name[0]) == false)) {
            $all_ok = false;
            $_SESSION['e_last_name'] = "Nazwisko musi rozpoczynać się wielką literą !";
        }

        $email = $_POST['email'];
        $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);

        if ((filter_var($emailB, FILTER_VALIDATE_EMAIL) == false) || ($emailB != $email)) {
            $all_ok = false;
            $_SESSION['e_email'] = "Podaj poprawny adres e-mail !";
        }

        if (!isset($_POST['regulamin'])) {
            $all_ok = false;
            $_SESSION['e_regulamin'] = "Zaakceptuj regulamin !";
        }

        $_SESSION['ok_login'] = $login;
        $_SESSION['ok_pass'] = $pass1;
        $_SESSION['ok_email'] = $email;
        $_SESSION['ok_first_name'] = $first_name;
        $_SESSION['ok_last_name'] = $last_name;
        if (isset($_POST['regulamin'])) $_SESSION['ok_regulamin'] = true;

        if ($all_ok == true) {

            require_once "connect.php";
            mysqli_report(MYSQLI_REPORT_STRICT);

            try {
                $connection = new mysqli($host, $db_user, $db_password, $db_name);
                if ($connection->connect_errno != 0) {
                    throw new Exception(mysqli_connect_errno());
                } else {
                    $result = $connection->query("SELECT * FROM users WHERE login = '$login'");

                    if (!$result) throw new Exception($connetion->error);

                    $login_count = $result->num_rows;
                    if ($login_count > 0) {
                        $row = $result->fetch_assoc();
                        if ($_SESSION['ok_pass'] == $row['password']) {
                            //update data if the user exists
                            if ($connection->query("UPDATE users SET first_name = '$first_name', last_name = '$last_name', email = '$email' WHERE login = '$login'")) {
                                $_SESSION['success_update'] = true;
                                //echo "Zaktualizowano usera";
                                header('Location: login.php');
                                exit();
                            }
                        } else {echo "Złe hasło. <a href='index2.php'> Spróbuj jeszcze raz. </a>";}

                    } else {
                        if ($connection->query("INSERT INTO users VALUES (NULL, '$login', '$pass1', '$first_name', '$last_name', '$email')")) {
                            $_SESSION['success_register'] = true;
                            //echo "Dodano usera";
                            header('Location: login.php');
                            exit();
                        } else {
                            throw new Exception($connetion->error);
                        }
                    }
                    $connection->close();
                }

            } catch (Exception $e) {
                echo '<span style="color:red;">Błąd serwera!</span>';
                //echo 'Info' . $e;
            }

        }
    }
?>
<!DOTYPE HTML>
<hmtl lang="pl">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <title>Strona główna</title>
        <style type="text/css">
            th{
                text-align: right;
            }
            h3 {
                text-align: center;
            }
        </style>
    </head>
    <body>
    <table cellpadding="5" cellspadding="10" align="center">
        <h3 align="center">Rejestracja</h3>
        <form method="post" >
            <tr><th>Login:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['ok_login']) && (!isset($_SESSION['e_login']))){
                        echo $_SESSION['ok_login'];
                        unset($_SESSION['ok_login']);
                    }?>" name="login"></td></tr>
            <tr><th>Hasło:</th><td><input type="password"  name="password1"></td></tr>
            <tr><th>Powtórz hasło:</th><td><input type="password" name="password2"></td></tr>
            <tr><th>Imie:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['ok_first_name']) && (!isset($_SESSION['e_first_name']))){
                        echo $_SESSION['ok_first_name'];
                        unset($_SESSION['ok_first_name']);
                    }?>" name="first_name"></td></tr>
            <tr><th>Nazwisko:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['ok_last_name']) && (!isset($_SESSION['e_last_name']))){
                        echo $_SESSION['ok_last_name'];
                        unset($_SESSION['ok_last_name']);
                    }?>" name="last_name"></td></tr>
            <tr><th>E-mail:</th><td><input type="text" value="<?php
                    if(isset($_SESSION['ok_email']) && (!isset($_SESSION['e_email']))){
                        echo $_SESSION['ok_email'];
                        unset($_SESSION['ok_email']);
                    }?>" name="email"></td></tr>
            <tr><th></th><td><label><input type="checkbox" name="regulamin"/> Akceptuję regulamin</label></td></th></tr>
            <tr><td colspan="2" align="right"><input type="submit" value="Zarejestruj się" name="submit"></td></tr>
        </form>
    </table>
    <?php
    if(isset($_SESSION['e_login'])) {
        echo $_SESSION['e_login'];
        unset($_SESSION['e_login']);
    }elseif (isset($_SESSION['e_pass'])){
        echo $_SESSION['e_pass'];
        unset($_SESSION['e_pass']);
    }elseif(isset($_SESSION['e_first_name'])){
        echo $_SESSION['e_first_name'];
        unset($_SESSION['e_first_name']);
    }elseif (isset($_SESSION['e_last_name'])){
        echo $_SESSION['e_last_name'];
        unset($_SESSION['e_last_name']);
    }elseif(isset($_SESSION['e_regulamin'])){
        echo $_SESSION['e_regulamin'];
        unset($_SESSION['e_regulamin']);
    }
    session_destroy();
    ?>
    </body>
</hmtl>
