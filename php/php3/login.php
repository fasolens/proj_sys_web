<?php
    session_start();

    if (!isset($_SESSION['success_edit'])) {

        if (isset($_SESSION['success_register']) || isset($_SESSION['success_update'])) {
            $_POST['login'] = $_SESSION['ok_login'];
            $_POST['password'] = $_SESSION['ok_pass'];
        }

        if ((empty($_POST['login'])) || (empty($_POST['password']))) {
            header('Location: index1.php');
            exit();
        }
    } else{
        $_POST['login'] = $_SESSION['login'];
        $_POST['password'] = $_SESSION['pass'];
    }

        //attach the file "connect.php" if it has not been previously attached
        require_once "connect.php";
        //new object mysqli
        $connetion = @new mysqli($host, $db_user, $db_password, $db_name);
        //connection failed
        if ($connetion->connect_errno != 0) {
            echo "Error: " . $connetion->connect_errno;
        } else {
            $login = $_POST['login'];
            $password = $_POST['password'];

            $login = htmlentities($login, ENT_QUOTES, "UTF-8");
            $password = htmlentities($password, ENT_QUOTES, "UTF-8");
            //check the correctness of the query
            if ($result = @$connetion->query(
                sprintf("SELECT * FROM users WHERE login='%s' AND password='%s'",
                    mysqli_real_escape_string($connetion, $login),
                    mysqli_real_escape_string($connetion, $password)))) {
                //num_rows - quantity of returned users
                $quantity_users = $result->num_rows;
                echo $quantity_users;
                if ($quantity_users > 0) {
                    //assoc array with data
                    $row = $result->fetch_assoc();
                    $_SESSION['login'] = $row['login'];
                    $_SESSION['pass'] = $row['password'];
                    $_SESSION['first_name'] = $row['first_name'];
                    $_SESSION['last_name'] = $row['last_name'];
                    $_SESSION['email'] = $row['email'];
                    echo "Udało nawiązac się połączenie";
                    $_SESSION['logged'] = true;
                    $session_id = session_id();
                    setcookie('session_id', $session_id, time() + 5 * 60);
                    $result->free_result();
                    header('Location: welcome.php');

                } else {
                    echo "Nie udało się nawiązać połączenia";
                }
                $connetion->close();
            }
        }
?>