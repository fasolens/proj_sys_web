<!DOCTYPE HTML>
<html lang="pl">
<head>
	<meta charset="utf-8" />
	<title>Witamy</title>
</head>

<body>

<?php 
	 
	$imie = $_POST['first_name'];
	$nazwisko = $_POST['last_name'];	
	$wiek = $_POST['age'];
	$wzrost = $_POST['height'];
	$waga = $_POST['weight'];
	$miesiac = $_POST['month'];
	$hello = "Witaj"; // Type dynamic
	$error = "<br> Nie udało się policzyć współczynnika BMI";

	if(empty($imie)&&empty($nazwisko))
	{
		//echo "Nie podano imienia ani nazwiska !!!";//It 
		header('Location: index.php');
		die();
	}
	else{

		//echo "$hello $imie $nazwisko";
		$array = array($hello, " ", $imie, " ", $nazwisko);

		for ($i = 0; $i < count($array); ++$i)
		{
			echo "$array[$i]";
		}


		if(empty($wzrost)||empty($waga)||$wzrost<0||$waga<0)
		{
			echo($error);

		} else{
			$wzrost_m = $wzrost/100;
			$bmi = $waga/($wzrost_m*$wzrost_m);
			echo "<br> BMI jako DOUBLE: ";
			echo round($bmi,1);
			settype($bmi, "integer"); //Convert to other data types
			echo "<br> BMI jako INTEGER: $bmi";
		}

		echo "<br>";

		// normal bmi ca.18-25
		define("bmi_l", 18); 
		define("bmi_h", 25);

		if(isset($bmi))
		{
			if($bmi<bmi_l) {
				echo "Powinieneś trochę przytyć";
			} elseif ($bmi>bmi_l && $bmi<bmi_h) {
				echo "Twoja waga jest w normie";
			} else {
				echo "Powinieneś trochę schudnąć";
			}
		}

		settype($miesiac, "string");
		$months_a = array("Styczeń" => "1", "Luty" => "2", "Marzec" => "3", "Kwiecień" => "4", "Maj" => "5", "Czerwiec" => "6",
						"Lipiec" => "7", "Sierpień" => "8", "Wrzesień" => "9", "Październik" => "10", "Listopad" => "11", "Grudzień" => "12");
		// strcmp return 0 if two words are equal
		foreach ($months_a as $element => $value) {
			if (!strcmp($miesiac, $value)) {
				echo "<br> Twój miesiąc urodzenia to: $element";
			}
		}

		echo "<br>===================================================================<br>";
		echo "Wypisz miesiące od stycznia do obecnego:";
		$months = array("Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień");

		$transdate = date('m-d-Y', time());
		$month_current = date('m'); //number of the current month
		for($i = 0; $i < $month_current; ++$i)
		{
			echo "<br> $months[$i]";
		}

		echo "<br>===================================================================<br>";
		echo "Numery miesięcy na literę L:<br>";
		while ($month_name = current($months)) {
			if($month_name[0] == 'L') {
				$m = key($months) + 1;
				echo $m.'<br>';
			}
			next($months);
		}

		echo "<br>===================================================================<br>";
		echo "Wyrażenie regularne <br>";

		$word = "'Talk is cheap. Show me the code!' -Linus Torvalds";
		print $word;

		if(preg_match("/Talk/", $word))
			print("<br> Znaleziono wyraz 'Talk' <br>");

		print "Znalezione słowo kończące się na 'ow': ";
		{	if(preg_match( "/\b([a-zA-Z]*ow)\b/i", $word, $match))
				print $match[1];

				$word = preg_replace("/".$match[1]."/","",$word);
				print "<br> Usuń słowo Show: ".$word;
		}
		echo "<br>===================================================================<br>";
		echo "Tablice superglobalne <br>";

		$ip = $_SERVER['REMOTE_ADDR'];

		function get_client_ip() {
			$ipaddress ='';
			if (isset($_SERVER['HTTP_CLIENT_IP']))
        		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		    else if(isset($_SERVER['HTTP_X_FORWARDED']))
		        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		    else if(isset($_SERVER['HTTP_FORWARDED']))
		        $ipaddress = $_SERVER['HTTP_FORWARDED'];
		    else if(isset($_SERVER['REMOTE_ADDR']))
		        $ipaddress = $_SERVER['REMOTE_ADDR'];
		    else
		        $ipaddress = 'UNKNOWN';
		    return $ipaddress;
		}

		
		//If it returned 1 then there is no address, I'm not sure
		echo "Adres ip ver1: ".$ip."<br>";
		//as above
		echo "Adres ip ver2: ".get_client_ip()."<br>";
		//Windows running IIS v6 does not include $_SERVER['SERVER_ADDR'], so if you need to get the IP addresse you should use this
		$ipAdd = gethostbyname($_SERVER['SERVER_NAME']);
		echo "Adres ip ver3: ".$ipAdd."<br>";
	}
?>

</body>
</html>