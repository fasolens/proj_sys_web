﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication4.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Customer name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Cusromer email is required")]
        public string Email { get; set; }

        [RegularExpression(@"^[0-9]{0,3}-[0-9]{9}|[0-9]{9,}", ErrorMessage = "Phone in xxx-123456789 or 123456789 format")]
        [Required]
        public string Phone { get; set; }
    }
}
