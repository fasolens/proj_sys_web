﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebApplication4.Models;

namespace WebApplication4.Pages
{
    public class ProductDetaisModel : PageModel
    {
        private readonly WebApplication4.Models.WebApplication4Context _context;

        public ProductDetaisModel(WebApplication4.Models.WebApplication4Context context)
        {
            _context = context;
        }

        public Product Product { get; set; }

        public async Task<IActionResult> OnGetAsync(Product p)
        {
            Product = p;

            return Page();
        }

        [HttpPost]
        public async Task<IActionResult> OnPostAsync(Product p)
        {
            Product = p;

            return Page();
        }
    }
}
