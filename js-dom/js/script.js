
document.writeln("<h1 id='title_h1'>Welcome to Javascript list</h1>");
document.writeln("<h2 id='authors'>Zieliński, Zborowski</h2>");

let name = "Guest";
let now = new Date();
let greetings;

// name = window.prompt("Please enter your name");

if (now.getHours() < 12 && now.getHours() > 5) {
    greetings = "Good morning";
} else if (now.getHours() > 18 && now.getHours() < 5) {
    greetings = "Good evening";
} else {
    greetings = "Good afternoon";
}
document.writeln("<p>" + greetings + " " + name + ", you are welcome!</p>");
document.writeln("<p>Current time is: " + now.getHours() + ":" + now.getMinutes() + "</p>");
let changing = false;

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function change_color() {
    let header = document.getElementById("header");
    header.innerHTML = "Color is changing at the moment.";
    let hsla = parseInt(Math.random() * 360);
    header.style.backgroundColor = "hsla(" + hsla + "0, 60%, 80%, 60%)";
    header.innerHTML = header.innerHTML + "<br>" + hsla;
}
async function change_color_start() {
    let timeout = 1000; // miliseconds
    changing = true;
    while (changing === true) {
        change_color();
        await sleep(timeout);
    }
}

function change_color_stop() {
    changing = false;
}
function show_alert() {
    window.alert("List of global variables")
}
let urlImage;
let testerDiv;
let textInput;
let domHolder;
let started = false;
let tableHolder;
let button111;
function start() {
    testerDiv = document.getElementById("tester");
    let rangeW = document.getElementById("rangeW");
    let rangeH = document.getElementById("rangeH");
    let colorSel = document.getElementById("colorSel");
    textInput = document.getElementById("textInput");
    rangeW.addEventListener("change", changeSizeW, false);
    rangeH.addEventListener("change", changeSizeH, false);
    colorSel.addEventListener("input", changeColor, false);
    textInput.addEventListener("input", changeText, false);

    domHolder = document.getElementById("dom_holder");
    let bAddButton = document.getElementById("create_element");
    let bCountButton = document.getElementById("count_everything");
    bAddButton.addEventListener("click", updateDom,false);
    bCountButton.addEventListener("click", countForms, false);

    tableHolder = document.getElementById("new_table");
    let bCreateTable = document.getElementById("create_table");
    bCreateTable.addEventListener("click", createRedTable, false);

    urlImage = document.getElementById("urlImage");
    button111.getElementById("button111").addEventListener("click",changeBackground, false);
}

function changeBackground() {
    let valueUrl = urlImage.value;
   document.body.style.background = "url("+ valueUrl + ") no-repeat";
}

window.addEventListener("load", changeBackground, false);

function changeText() {
    testerDiv.innerHTML = textInput.value;
}
function changeColor(event) {
    testerDiv.style.backgroundColor = event.target.value;
}
function changeSizeW(event) {
    let wField = document.getElementById("width");
    wField.innerHTML = event.target.value;
    testerDiv.style.width = event.target.value + "px";
}
function changeSizeH(event) {
    let hField = document.getElementById("height");
    hField.innerHTML = event.target.value;
    testerDiv.style.height = event.target.value + "px";
}


function updateDom() {
    let textInput = document.getElementById("inputName");
    if (started === false) {
        domHolder.innerHTML = "";
        started = true;
    }
    createButton(textInput.value);
    textInput.focus();
}
function createButton(label) {
    let btn = document.createElement('button');
    let l = document.createTextNode(label);
    let radiosWhereValue = document.querySelector('input[name="where"]:checked').value;
    let position = document.getElementById('inputPosition');
    btn.appendChild(l);
    switch(parseInt(radiosWhereValue)) {
        case 1:
            domHolder.appendChild(btn);
            break;
        case 2:
            addBefore(btn, position.value);
            break;
        case 3:
            addReplace(btn, position.value);
            break;
        case 4:
            remove(position.value);
            break;
        case 5:
            getParent();
            break;
        default:
            window.alert("Wybierz opcje!")
    }
}
function addBefore(button, position) {
    if (domHolder.children.length <= position) {
        window.alert("Błąd pozycji!");
    } else {
        domHolder.insertBefore(button, domHolder.childNodes[position]);
    }
}
function addReplace(button, position) {
    if (domHolder.children.length <= position) {
        window.alert("Błąd pozycji!");
    } else {
        domHolder.replaceChild(button, domHolder.childNodes[position]);
    }
}
function remove(position) {
    if (domHolder.children.length <= position) {
        window.alert("Błąd pozycji!");
    } else {
        domHolder.removeChild(domHolder.childNodes[position]);
    }
}
function countForms() {
    let cForms = document.forms.length;
    document.getElementById("forms_count_div").innerHTML = "Liczba formularzy: " + cForms;
}
function getParent() {
    let parentTag = domHolder.parentNode.tagName;
    document.getElementById("forms_count_div").innerHTML = "Parent node to: " + parentTag;
}


function createRedTable() {
    let table = document.createElement("table");
    for (i = 0; i <= 255; i++) {
        table.appendChild(createRow(i));
    }
    tableHolder.appendChild(table);
}

function createRow(redValue) {
    let row = document.createElement("tr");
    let col = document.createElement("td");
    col.style.backgroundColor = "rgba(" + redValue + ", 0, 0, 1)";
    col.style.width = "100px";
    col.style.height = "25px";
    col.innerHTML = redValue;
    row.appendChild(col);
    return row
}

window.addEventListener("load", start, false);

function mouseFun(e) {
    var x = e.clientX;
    var y = e.clientY;
    var wsp = "Współrzędne: (" + x + "," + y + ")";
    document.getElementById("pmouse").innerHTML = wsp;
}

function clearCoor() {
    document.getElementById("pmouse").innerHTML = "";
}

